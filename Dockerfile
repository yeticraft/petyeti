

# What base image to use
FROM python:3

# Any environmental settings/values that are needed withon the container
ENV PYTHONUNBUFFERED 1

# Where we will be working with our code withon the container (creates the directory for us, also)
WORKDIR /code

# Setup steps
COPY requirements.txt /code/
RUN pip install --upgrade wheel && pip install -r requirements.txt

# Copy the folder into the container
COPY . /code/

# Start bpython
CMD ['/usr/local/bin/python3','/code/petcmd','-S']
