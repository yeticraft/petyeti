import random
import string

def newname():
    name = ""

    for x in range(2):
        name = name + random.choice(string.ascii_uppercase)
    for x in range(3):
        name = name + chr(random.randrange(48,57))
    return name

def modifyStat(stat, amount, modifier=1):
    return stat + (amount * modifier)

Foods = {
    "Meat" : 5,
    "Vegtable" : 4,
    "Fruit" : 3,
    "Candy" : 2
}

Washes = {
    "Simple" : 1,
    "Basic" : 2,
    "Extream" : 3
}

class Pet(object):

    def __init__(self):
        
        # Name
        self.name = newname()
        
        # Stats
        self.hunger = 0
        self.emotion = 0
        self.cleanliness = 0
        self.health = 0

        # Modifiers
        self.hungerModifier = 1

    def reset(self):
        name = self.name
        while name == self.name:
            self.name = newname()
            
    #
    # HUNGER/FOOD
    #
    def DecreaseHunger(self):
        hunger = modifyStat(self.hunger, -1, self.hungerModifier)
        self.hunger = hunger
    
    def EatFood(self, food):
        hunger = modifyStat(self.hunger, Foods[food])
        self.hunger = hunger

    def GetHungerLevel(self):
        hunger = self.hunger
        
        if (hunger >= -15 and hunger < -10): return "Starving"
        if (hunger >= -10 and hunger < -5): return "Hungry"
        if (hunger >= -5 and hunger < 5): return "Satisfied"
        if (hunger >= 5 and hunger < 10): return "Full"
        if (hunger >= 10 and hunger <15): return "Stuffed"

    #
    # CLEANLINESS
    # 
    def GetCleanlinessLevel(self):
        cleanliness = self.cleanliness

        return cleanliness
    
    def CleanPet(self, wash):
        cleanliness = modifyStat(self.cleanliness, Washes[wash])
        self.cleanliness = cleanliness
    
    def DirtyPet(self, amount):
        cleanliness = modifyStat(self.cleanliness, amount)
        self.cleanliness = cleanliness

if __name__ == "__main__":
    mypet = Pet()
    print(mypet.name)