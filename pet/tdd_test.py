import unittest
import random

from pet import Pet

class PetNameTest(unittest.TestCase):
    # assertRegex() alias to address DeprecationWarning
    # assertRegexpMatches got renamed in version 3.2
    if not hasattr(unittest.TestCase, "assertRegex"):
        assertRegex = unittest.TestCase.assertRegexpMatches

    name_re = r'^[A-Z]{2}\d{3}$'

    def test_has_name(self):
        self.assertRegex(Pet().name, self.name_re)

    def test_name_sticks(self):
        pet = Pet()
        pet.name
        self.assertEqual(pet.name, pet.name)

    def test_different_robots_have_different_names(self):
        self.assertNotEqual(
            Pet().name,
            Pet().name
        )

    def test_reset_name(self):
        # Set a seed
        seed = "Totally random."

        # Initialize RNG using the seed
        random.seed(seed)

        # Call the generator
        pet = Pet()
        name = pet.name

        # Reinitialize RNG using seed
        random.seed(seed)

        # Call the generator again
        pet.reset()
        name2 = pet.name
        self.assertNotEqual(name, name2)
        self.assertRegex(name2, self.name_re)

class PetHungerTests(unittest.TestCase):

    def test_can_feed_pet(self):
        hungerLevel = "Full"

        myPet = Pet()
        myPet.EatFood("Meat")

        self.assertIs(myPet.GetHungerLevel(), hungerLevel)

    def test_has_hunger(self):
        self.assertIs(Pet().hunger, 0)

    def test_hunger_decreasing(self):
        hungerLevel = "Hungry"

        myPet = Pet()
        for i in range(6):
            myPet.DecreaseHunger()
        
        self.assertIs(myPet.GetHungerLevel(),hungerLevel)    
    
    def test_can_be_Starving(self):
        hungerLevel = "Starving"

        myPet = Pet()
        for i in range(12):
            myPet.DecreaseHunger()
        
        self.assertIs(myPet.GetHungerLevel(), hungerLevel)

class PetCleanlinessTests(unittest.TestCase):
    
    def test_has_cleanliness(self):
        myPet = Pet()

        self.assertGreater(myPet.GetCleanlinessLevel(), -15)
    
    def test_can_pet_be_cleaned(self):
        myPet = Pet()
        initial_cleanliness = myPet.GetCleanlinessLevel()
        myPet.CleanPet("Basic")
        final_cleanliness = myPet.GetCleanlinessLevel()

        self.assertGreater(final_cleanliness, initial_cleanliness)

    def test_can_pet_get_dirty(self):
        myPet = Pet()
        initial_cleanliness = myPet.GetCleanlinessLevel()
        myPet.DirtyPet(-4)
        final_cleanliness = myPet.GetCleanlinessLevel()

        self.assertLess(final_cleanliness, initial_cleanliness)

if __name__ == "__main__":
    unittest.main()
