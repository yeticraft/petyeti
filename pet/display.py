#!/usr/bin/env python3

"""
    Display
    ~~~~~~~~~

    A switchable object generator for selecing the output display for the pet.
    Default will be the simulator for safe debugginh and testing, however it will not
    output anything - it will merely act as stubcode in place of a Inky or emulated displays.
    Setting the active display to `emulated` will respond with text responces for debugging.
    Setting the active display to `inky` is expected for normal operation on the hardware.
"""

import logging

DISPLAY_SIMULATED = 0
DISPLAY_INKY = 1
DISPLAY_EMULATED = 2

DISPLAYS = {
    'simulated': DISPLAY_SIMULATED,
    'inky': DISPLAY_INKY,
    'emulated': DISPLAY_EMULATED,
}

####TODO: create mapping data for displays that include the color capability, dimensions, etc


class Display():

    def __init__(self, displaytype=0, color='red'):

        self.display = None
        self.displaytype = displaytype
        self.color = color
        self.set_display(self.displaytype)

    def set_display(self, displaytype=0):

        if displaytype == 0:
            from .InkyEmulator import inkyemulator
            self.display =  inkyemulator() ####TODO: create a text output shim
        elif displaytype == 1:
            from inky import InkyPHAT
            inky_display = InkyPHAT("red")
            self.display = inky_display
        else:
            self.display =  None  ####TODO: create a text output shim
    

if __name__ == '__main__':

    disp = Display(0)
    disp.set_display(DISPLAY_SIMULATED)
