from PIL import Image

class inkyemulator():

    def __init__(self):
    
        self.image = None
    
    def set_image(self, image):
    
       self.image = image

    def show(self):
        
        self.image.show()
    