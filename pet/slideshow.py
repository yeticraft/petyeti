#!/usr/bin/env python3

"""
    Slideshow
    ~~~~~~~~~

    A demonstration library for the IoT AI Pet with Raspberry Pi and E-Ink screen

"""

from time import time, sleep
from PIL import Image
from os import listdir
from os import getcwd

from .display import DISPLAY_SIMULATED

class Slideshow():

    def __init__(self, display, media="/media/slides"):

        self.image_location = getcwd() + media ####TODO: validate the directory passed to us before using it
        self.display = display

    def get_slides(self):
        return( listdir(self.image_location))
    
    def play_slides(self):
        file_list = self.get_slides()
        for image in file_list:

            img = Image.open(self.image_location + '/' + image)
            self.display.display.set_image(img)
            self.display.display.show()

            starttime = time()
            sleep(5.0 - ((time() - starttime) % 5.0))


if __name__ == '__main__':

    show = Slideshow()
    show.play_slides()
