# PetYeti

Home for all the main sourcecode, installers, and etc for the PetYeti "personal pet yeti" AI driven toy that will run on a RPiZW with an e-ink screen.

We will be posting images of mockups (and prototypes) as well as design ideas and psudo-code that will help us refine and clarify the initial implementation of the gadget and software.  
We have (mostly) nailed down the crucial starter-point and have the prototype to a beta level, so we have taken this project public and invite any/all commers to participate with PRs etc.
Please remember this warning in your checkins though - **do NOT _ever_ include passowrds or auth tokens in your code.**  Do not store hardcoded passwords or private keys in the project either.  Anything pushed to the repository will evenntually be facing the public view - so be warned.

## Setup and Installation

Clone this repo or copy the code to a linux system, then open a terminal shell in the directory before executing the following command:

    python3 -m venv .py3
    . .py3/bin/activate

This will create a local Python3 virtual environment that allows isolation from the system's copy and switch your python to this version.

Next, make sure that you have the Python3 Developer libraries installed, and then install the project python libraries like so:

    pip install --upgrade pip wheel
    pip install -r requirements.txt

## Usage

Execute the command-line interface tool like so (assuming a linux based, python3 environment with `requirements.txt` installed correctly).

```bash
./petcmd -h
```

## Developer Notes

The following are mostly links and random bits of info that the developers thought were important or needed to share with one another.

- https://sourceforge.net/projects/raspberry-gpio-python/ - library to alllow using the code on a NON-Raspberry Pi during development.
- https://blog.withcode.uk/wp-content/uploads/2016/10/RPi_GPIO_python_quickstart_guide.pdf
- https://gitlab.com/shezi/GPIOSimulator - gpiosimulator- https://docs.python.org/3/howto/argparse.html - Option parsing library for commanline processing
- https://docs.python.org/3/library/configparser.html - configuration file library
- https://docs.python.org/3/howto/logging.html - howto for logging with python
- https://code.visualstudio.com/docs/python/testing - adding tests to python code with Visual Code
- https://code.visualstudio.com/download - Get Visual Code
- https://realfavicongenerator.net/ - favicon generator
